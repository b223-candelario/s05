<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05: Client-Server Communication (Basic To-Do List)</title>
</head>
<body>
	<!-- 
		Session
			- A session is a way to store information (in variables) to be used across multiple pages.
			- Session variables hold information about one single user, and are available to all pages in one application.
	-->

	<!-- Start the session and must be he very first thing your document before any HTML tags.  -->

	<?php session_start(); ?>

	<h3>Add Task</h3>
	<!-- "action attribute" contains the destination/URL endpoint.-->

	<!-- Create Task -->
	<form method="POST" action="./server.php">
		<!-- This will identify the action or type of request of a user. -->
		<input type="hidden" name="action" value="ADD" />
		Description: <input type="text" name="description" required />
		<button type="submit">ADD</button>
	</form>

	<h3>Task List</h3>
	<?php if(isset($_SESSION['tasks'])): ?>
		<?php foreach($_SESSION['tasks'] as $index => $task): ?>
			<div>
				<!-- Update Task-->
				<!-- The following can be updated in the task:
					- description
					- status (complete or not) -->
				<form method="POST" action="./server.php" style="display: inline-block;">
					<input type="hidden" name="action" value="UPDATE" />
					<input type="hidden" name="id" value="<?php echo $index; ?>" />
					<input type="checkbox" name="isFinished" <?php echo ($task->isFinished) ? 'checked' : null; ?> />
					<input type="text" name="description" value="<?php echo $task->description; ?>">
					<input type="submit" name="" value="UPDATE">
				</form>

				<!-- Delete a TASK -->
				<form method="POST" action="./server.php" style="display: inline-block;">
					<input type="hidden" name="action" value="DELETE" />
					<input type="hidden" name="id" value="" />
					<input type="submit" name="" value="DELETE" />
				</form>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	<br><br>

	<!-- Clear all the task. -->
	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="CLEAR">
		<button type="submit">CLEAR ALL TASKS</button>
	</form>
</body>
</html>