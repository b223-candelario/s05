<?php 
	// $_GET and $_POST are super global variables in PHP.
		// Always accessible regardless the scope.
		// Super global variables allows data to persist between pages or a single session.
		// Both createan array that holds key/value pairs, where:
			// "key" represents the name of the form control element.
			// "values" represent the input data from the user.
	// isset() function checks wether a variable is set.
		// returns true if variable is set otherwise false
	
	$task = ["Get git", "Bake HTML", "Eat CSS", "Learn PHP"];

	if(isset($_GET['index'])){
		$indexGet = $_GET['index'];
		echo "The retrieved task from GET is $task[$indexGet]
			  </br>";
	}
	if(isset($_POST['index'])){	
		$indexPost = $_POST['index'];
		echo "The retrieved task from POST is $task[$indexPost]
			  </br>";
	}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S05: Client-Server Communication (GET and POST)</title>
	</head>
	<body>
		<h1>Task index from GET</h1>
		<form method="GET">

			<!-- Email: <input type="text" name="email">
			password: <input type="password" name="password"> -->

			</br>

			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
			<button type="submit">GET</button>
		</form>

		<h1>Task index from POST</h1>
		<form method="POST">
			<select name="index" required>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
			</select>
			<button type="submit">POST</button>
		</form>
	</body>
</html>