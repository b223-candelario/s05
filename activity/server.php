<?php 
	session_start();

class Login{
	public function logIn($email, $password){
		if($email === 'johnsmith@gmail.com' && $password === '1234'){
			$_SESSION['email'] = $email;
		}else{
			$_SESSION['login_error_message'] = 'Incorrect username or password';
		}
	}

	public function logOut(){
		session_destroy();
	}
}

$login = new Login();

if($_POST['action'] === 'LOGIN'){
	$login->logIn($_POST['email'], $_POST['password']);
}else if($_POST['action'] === 'LOGOUT'){
	$login->logOut();
}

header('Location: ./index.php')

?>