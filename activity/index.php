<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05 Activity: Client-Server Communication (Login)</title>
</head>
<body>
	<?php session_start() ?>
	<?php if(!isset($_SESSION['email'])): ?>
		<h3>LOG IN</h3>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="LOGIN">
			<table>
				<tr>
					<td>Email:</td>
					<td><input type="email" name="email" required></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type="Password" name="password" required></td>		
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" name="" value="Login"></td>
				</tr>
			</table>		
		</form>
		<?php
			if(isset($_SESSION['login_error_message'])){
				echo $_SESSION['login_error_message'];
			} 
		?>
	<?php endif; ?>
	<?php if(isset($_SESSION['email'])): ?>
		<p><?php echo "Hello, " . $_SESSION['email'] . "!"; ?></p>
		<form method="POST" action="./server.php">
			<input type="hidden" name="action" value="LOGOUT">
			<button type="submit">Log out</button>
		</form>
	<?php endif; ?>
</body>
</html>